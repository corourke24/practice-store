class Product < ApplicationRecord
    validates :title, :description, :image_url, presence: true
    validates :title, uniqueness: true
    validates :price, numericality: {greater_than_or_equal_to: 0.01 }
    IMAGE_REGEX = %r{\.(gif|jpg|png)\Z}i
    validates :image_url, allow_blank: true, format: {
        with: IMAGE_REGEX,
        message: 'Must ve a URL, GIF, JPG, PNG or image'
    }
end
