require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  fixtures :products
  test "Product fields must not be empty" do
    product = Product.new
    assert product.invalid?
    assert product.errors[:title].any?
    assert product.errors[:description].any?
    assert product.errors[:price].any?
    assert product.errors[:image_url].any?
  end
  
  test "Product price must be positive" do
    product = Product.new(
      title: "My title",
      description: "yyy",
      image_url: 'yyy.jpg'
    )
    product.price = -1
    assert product.invalid?
    assert_equal ["must be greater than or equal to 0.01"], product.errors[:price]
    
    product.price = 1
    assert product.valid?
  end
  
  
  # Setup image_url test
  def new_product(image_url)
    Product.new(
      title: "My title",
      description: 'test description',
      price: 1,
      image_url: image_url
    )
  end
  
  test "image_url must be valid image or image url" do
    corrects = %w{ fred.gif fred.jpg fred.png FRED.JPG http://a.b.c/x/y/zfred.gif }
    incorrects = %w{ fred.doc fred.gif/more fred.gif.more }
    
    corrects.each do |c|
      assert new_product(c).valid?, "#{c} shouldn't be invalid"
    end
    
    incorrects.each do |i|
      assert new_product(i).invalid?, "#{i} shouldn't be valid"
    end
  end
  
  test "product should have unique name" do
    product = Product.new(
      title: products(:ruby).title,
      description: "product description",
      price: 1,
      image_url: "image.png"
      )
      assert product.invalid?
      assert_equal [I18n.translate('errors.messages.taken')], product.errors[:title]
    end
end
