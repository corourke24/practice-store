Product.delete_all

Product.create(
    title: 'Test product 1',
    description: %{<p>
        <em>Dank dannys dank deal</em>
        More info in general
        </p>},
    image_url: 'telluscircle1.png',
    price: 26.00)

Product.create(
    title: 'Test product 2',
    description: %{<p>
        <em>Dank dannys dank deal</em>
        More info in general
        </p>},
    image_url: 'telluscircle2.png',
    price: 28.00)

Product.create(
    title: 'Test product 3',
    description: %{<p>
        <em>Dank dannys dank deal</em>
        More info in general
        </p>},
    image_url: 'telloweenposter.jpg',
    price: 28.00)
